import skimage.io as io
import sknw
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
import numpy as np
import networkx as nx
import scipy.stats as stats

from math import *

skeleton = io.imread('/home/markov/data/cellmask/176_skeleton.tif')
graph = sknw.build_sknw(skeleton)

minedge = 10
shift = 3

plt.imshow(skeleton)

# remove small edges
toremove = []
for (s,e) in graph.edges():
    if graph[s][e]['weight'] < minedge:
        toremove.append((s,e))

graph.remove_edges_from(toremove)
print('Filter ttubules by length:', len(toremove), 'ttubules removed; ', len(graph.edges()), 'ttubules kept')

# calculate stats
bearing = []
colb = plt.cm.jet(np.linspace(0,1,91))
for (s,e) in graph.edges():
    ps = graph[s][e]['pts']

    for i in range(shift, len(ps)-shift):
        v = ps[i-shift:i+shift+1, :]
        par = np.arange(v.shape[0])
        slope_x, intercept_x, r_value_x, p_value_x, std_err_x = stats.linregress(par,v[:,1])
        slope_y, intercept_y, r_value_y, p_value_y, std_err_y = stats.linregress(par,v[:,0])
        dx = slope_x
        dy = slope_y
        l = sqrt(dx*dx+dy*dy)
        b = 180/pi*atan2( abs(dy), abs(dx) )
        bearing.append([b, l])
        #plt.plot(v[:,1], v[:,0], color=colb[int(b)])

#plt.colorbar(mappable=cm.ScalarMappable(norm=colors.Normalize(0,90), cmap=plt.cm.jet))

# cnt = defaultdict(int)
# for _, nbrs in graph.adj.items():
#     nk = len(nbrs.keys())
#     if nk > 0:
#         for __, edge in nbrs.items():
#             cnt[__] += 1
#             ps = edge['pts']
#             plt.plot(ps[:,1], ps[:,0], 'green')
#             plt.plot([ps[0,1]], [ps[0,0]], 'r.')

# print(cnt)

# # draw edges by pts
# for (s,e) in graph.edges():
#     ps = graph[s][e]['pts']
#     plt.plot(ps[:,1], ps[:,0], 'green')
    
# # draw node by o
# nodes = graph.nodes()
# ps = np.array([nodes[i]['o'] for i in nodes])
# plt.plot(ps[:,1], ps[:,0], 'r.')

# # for i in nodes:
# #     p = nodes[i]['pts']
# #     plt.plot(p[:,1], p[:,0], 'bx')


bearing = np.array(bearing)

print(bearing[:,0].min(), bearing[:,0].max())

plt.figure()
bhisto, bin_edges = np.histogram(bearing[:,0], bins=10, weights = bearing[:,1], density=True)
bin_edges = (bin_edges[:-1]+bin_edges[1:])/2
#plt.gca().set_ylim(bottom=0)
plt.hist(bearing[:,0], bins=5, weights = bearing[:,1], density=True)
plt.plot(bin_edges, bhisto, '.-')
plt.show()
