#!/usr/bin/env python3

import argparse
import keyring, sys, os
import numpy as np
import getpass
import json
import subprocess

import omero.model
from omero.gateway import BlitzGateway
from omero.rtypes import rdouble, rint, rstring

import skimage.io as io
import skimage.measure as measure
import skimage.morphology as morphology
import scipy.ndimage as ndimage
import scipy.stats as stats

import sknw

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors

import PIL

from math import *

keyring_help = """
To set password for your login into OMERO, use command keyring:

keyring set omero username@host

where username is your OMERO username and host is OMERO host. For example,

keyring set omero user@omero.sysbio

While running the command, you should be prompted for the password.

"""

parser = argparse.ArgumentParser(description='Detect T-Tubules',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog=keyring_help)

parser.add_argument('imageid', type=int, help='Image ID in OMERO database')
parser.add_argument('--host', default='omero.sysbio', help='OMERO host')
parser.add_argument('--username', default=getpass.getuser(), help='OMERO user login name')
parser.add_argument('--channel', type=int, default=0, help='Channel number, starting from zero')
parser.add_argument('--timepoint', type=int, default=0, help='Timepoint number, starting from zero')
parser.add_argument('--dilation', type=int, default=1, help='Dilation applied before skeletonization of the segmented image')
parser.add_argument('--minsize', type=int, default=16, help='Minimal size in pixels of a t-tubule network segment')
parser.add_argument('--minedge', type=int, default=5, help='Minimal length in pixels of a t-tubule network skeleteon edge')
parser.add_argument('--bearing-half-segment', type=int, default=3,
                    help='Size of a half-segment used to calculate bearing of ttubule. ' +
                    'Do not change unless you know what you are doing')
parser.add_argument('--bearing-bins', type=int, default=5,
                    help='Number of bins used to calculate bearing distribution histogram')
parser.add_argument('--ilastik', required=True, help='Ilastik project file')
parser.add_argument('--output', required=True, help='Output folder for intermediate files')
parser.add_argument('--clear-rois', action='store_true', help='Wipe ttubules already attached to the image')
parser.add_argument('--omero', action='store_true', help='Store detected t-tubules as ROIs on OMERO server')
parser.add_argument('--plot', action='store_true', help='Show plots describing detection')
parser.add_argument('--save-merged', action='store_true', help='Save merged image')

args = parser.parse_args()

host = args.host
username = args.username
imageId = args.imageid
channel = args.channel
timepoint = args.timepoint
dilation = args.dilation
minsize = args.minsize
minedge = args.minedge
bearing_half_segment = args.bearing_half_segment
bearing_bins = args.bearing_bins
ilastik = args.ilastik
output_dir = args.output

# internal vars
keyring_key = "omero"
cache_file = ".cache.image.npz"
ttubules_area_label = "ttubules area"
ttubules_roi_label = 'ttubule'

def checkCache(image):
    Id = image.getId()
    print('Checking cache for image', Id)
    d = None
    try:
        t = np.load(cache_file)
        if t['id'] == Id and t['channel'] == channel and t['timepoint'] == timepoint:
            d = t['data']
            print('Data loaded from cache')
    except:
        print('No cache found')
    return d

def saveToCache(image, data):
    Id = image.getId()
    np.savez(cache_file, id=Id, channel=channel, timepoint=timepoint, data=data)

def getData(image):
    data = checkCache(image)
    if data is not None: return data
    pixels = image.getPrimaryPixels()
    sz = image.getSizeZ()
    planes = pixels.getPlanes([(zi, channel, timepoint) for zi in range(sz)])
    data = []
    print("Loading", image.getName(), end=": ")
    for p in planes:
        print(".", end="", flush=True)
        data.append(np.array(p))
    print(" done")
    if len(data) != 1:
        raise RuntimeError('Only single plane images are supported')
    data = data[0]
    saveToCache(image, data)
    return data

def getPoints(shape):
    s = str(shape.getPoints().getValue())
    v = []
    for p in s.split():
        a = tuple([int(k) for k in p.split(',')])
        v.append(a)
    return v #np.array(v)

def deleteObjects(objType, IDs):
    print('Deleting %s: ' % objType, end="", flush=True)
    handle = conn.deleteObjects(objType, IDs, deleteChildren=True)
    cb = omero.callbacks.CmdCallbackI(conn.c, handle)
    while not cb.block(500): print(".", end="", flush=True)
    print()
    err = isinstance(cb.getResponse(), omero.cmd.ERR)
    if err:
        print(cb.getResponse())
        raise Exception('Failed to delete %s. Exiting.' % objType)
    else:
        print('%s deleted' % objType)

####################################################
# main
####################################################

os.makedirs(output_dir, exist_ok=True)
basename = os.path.join(output_dir,str(imageId))

password = keyring.get_password(keyring_key, "%s@%s" % (username, host))
if password is None:
    print('\nPlease set login password using keyring')
    print(keyring_help)
    print("""In your case, username is currently set to '%s' and host is '%s'. So,
it should be

keyring set omero %s@%s

""" % (username, host, username, host))
    sys.exit(2)

print('Connecting as', username)
conn = BlitzGateway(username, password, host=host, secure=True)
conn.connect()

roi_service = conn.getRoiService()
updateService = conn.getUpdateService()

image = conn.getObject("Image", imageId)
print('Image:', image.getName(), " Dimensions: %d x %d x %d; channels=%d timepoints=%d" %
      (image.getSizeX(), image.getSizeY(), image.getSizeZ(), image.getSizeC(),image.getSizeT()))


data = getData(image)

# get ttubule search area ROI and check for predefined ROIs
result = roi_service.findByImage(imageId, None)
ttubules_area = None
ttubules_rois = set()
for roi in result.rois:
    for s in roi.copyShapes():
        if type(s) == omero.model.PolygonI and \
           s.getTextValue().getValue() == ttubules_area_label:
            ttubules_area = getPoints(s)
            print('TTubules search area specified by ROI', s.getId().getValue())
        elif type(s) == omero.model.PolylineI and \
           s.getTextValue().getValue() == ttubules_roi_label:
            ttubules_rois.add(roi.getId().getValue())

if ttubules_area is None:
    print('Error: ttubules search area is not specified as ROI. Select an area as a polygon and name it via comment as "%s"' % ttubules_area_label)
    conn.close()
    sys.exit(-2)

if args.omero:
    if len(ttubules_rois) > 0 and not args.clear_rois:
        print('You have ROIs defined already for the image. For safety, stopping here.',
              'If you wish to clear ROIs and re-detect the centers, use option --clear-rois')
        conn.close()
        sys.exit(-1)

    if len(ttubules_rois) > 0:
        deleteObjects('Roi', list(ttubules_rois))


with PIL.Image.new('L', data.T.shape, 0) as img:
    PIL.ImageDraw.Draw(img).polygon(ttubules_area, fill=1, outline=1)
    ttubules_mask = np.array(img)


# plt.imshow(data)
# plt.imshow(data * ttubules_mask)
# plt.show()


# store original
io.imsave('%s.tif' % basename, data)

subprocess.run(["ilastik", "--headless", "--project=" + ilastik,
                "--export_source=Simple segmentation", "--output_format=tif",
                "--output_filename_format=%s/{nickname}_segmented.tif" % output_dir,
                '%s.tif' % basename], check = True)

segmented = io.imread('%s_segmented.tif' % basename)
segmented = segmented > segmented.min()

if dilation > 0:
    print('Dilation:', dilation, 'pixel(s)')
    segmented = morphology.binary_dilation(segmented, morphology.square(2*dilation + 1))

# filter by size
labels, lmax = measure.label(segmented, return_num=True)
segmented_use = np.zeros(segmented.shape)
segmented_drop = np.zeros(segmented.shape)
cnt_use, cnt_drop = 0, 0
for i in range(1, lmax+1):
    sz = np.sum(labels == i)
    if sz > minsize:
        segmented_use[labels == i] = True
        cnt_use += 1
    else:
        segmented_drop[labels == i] = True
        cnt_drop += 1
print('Sectors kept and dropped according to size: %d kept; %d dropped' % (cnt_use, cnt_drop))

# calc skeleton
skeleton = morphology.skeletonize(segmented_use)
skeleton = skeleton * ttubules_mask

# transfer skeleton to graph
graph = sknw.build_sknw(skeleton)

# remove small edges
toremove = []
for (s,e) in graph.edges():
    if graph[s][e]['weight'] < minedge:
        toremove.append((s,e))

graph.remove_edges_from(toremove)
print('Filter ttubules by length:', len(toremove), 'ttubules removed; ', len(graph.edges()), 'ttubules kept')

# merged image
factor = np.percentile(data, 99)
img = np.dstack([np.clip(data / factor, 0, 1.0), skeleton*1.0, np.zeros(data.shape)])

# save results
io.imsave('%s_skeleton.tif' % basename, skeleton)
if args.save_merged:
    io.imsave('%s_merged.tif' % basename, img)

if args.omero:
    print('Creating ROIs in OMERO')
    rois = []
    for (s,e) in graph.edges():
        line = omero.model.PolylineI()
        ps = graph[s][e]['pts']
        points = " ".join(["%i,%i" % (ps[i,1], ps[i,0]) for i in range(len(ps))])
        line.points = rstring(points)
        # line.theZ = rint(0)
        # line.theT = rint(0)
        line.textValue = rstring(ttubules_roi_label)
        roi = omero.model.RoiI()
        roi.setImage(image._obj)
        roi.addShape(line)
        rois.append(roi)
        # if len(rois) > 0:
        #     updateService.saveArray([rois])
        #     rois = []
        updateService.saveAndReturnObject(roi)

    # if len(rois) > 0:
    #     updateService.saveArray([rois])

    # for (s,e) in graph.edges():
    #     line = omero.model.PolylineI()
    #     ps = graph[s][e]['pts']
    #     points = " ".join(["%i,%i" % (ps[i,1], ps[i,0]) for i in range(len(ps))])
    #     line.points = rstring(points)
    #     # line.theZ = rint(0)
    #     # line.theT = rint(0)
    #     # print(points)
    #     line.textValue = rstring(ttubules_roi_label)
    #     rois.append(line)

    # if len(rois) > 0:
    #     roi = omero.model.RoiI()
    #     roi.setImage(image._obj)
    #     for r in rois:
    #         roi.addShape(r)
    #     updateService.saveArray([roi])
    #     #updateService.saveAndReturnObject(roi)

if args.plot:
    # plot analysis steps
    io.imshow_collection([data, segmented, segmented_use, segmented_drop, skeleton, img])

    # merged image
    fig = plt.figure()
    my_cmap = cm.gray
    my_cmap.set_under('k', alpha=0)

    plt.imshow(data)
    plt.imshow(skeleton*1, cmap=my_cmap, interpolation='none', clim=[0.9, 1])

    plt.show()

# Close OMERO connection
conn.close()
