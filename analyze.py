#!/usr/bin/env python3

import argparse
import keyring, sys, os
import numpy as np
import getpass
import json
import subprocess

import omero.model
from omero.gateway import BlitzGateway
from omero.rtypes import rdouble, rint, rstring

import skimage.io as io
import skimage.measure as measure
import skimage.morphology as morphology
import scipy.ndimage as ndimage
import scipy.stats as stats
import scipy.signal as signal
from scipy.signal import find_peaks

import sknw

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors

import PIL

import fracdim as fr

import random

from iocbio.kinetics.io.db import DatabaseInterface

from math import *

keyring_help = """
To set password for your login into OMERO, use command keyring:

keyring set omero username@host

where username is your OMERO username and host is OMERO host. For example,

keyring set omero user@omero.sysbio

While running the command, you should be prompted for the password.

"""

parser = argparse.ArgumentParser(description='Analyze T-Tubules',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog=keyring_help)

parser.add_argument('imageid', type=int, help='Image ID in OMERO database')
parser.add_argument('--host', default='omero.sysbio', help='OMERO host')
parser.add_argument('--username', default=getpass.getuser(), help='OMERO user login name')
parser.add_argument('--bearing-half-segment', type=int, default=3,
                    help='Size of a half-segment used to calculate bearing of ttubule. ' +
                    'Do not change unless you know what you are doing')
parser.add_argument('--bearing-bins', type=int, default=5,
                    help='Number of bins used to calculate bearing distribution histogram')
parser.add_argument('--minedge', type=int, default=8,
                    help='Minimal length in pixels of a t-tubule network skeleteon edge. Should be the same as used for detection.')
parser.add_argument('--plot', action='store_true', help='Show statistical plots')
parser.add_argument('--plot-bearing', action='store_true', help='Show bearing for ttubules')
parser.add_argument('--no-save', action='store_true', help='Don\'t save data to the database')

args = parser.parse_args()

host = args.host
username = args.username
imageId = args.imageid
bearing_half_segment = args.bearing_half_segment
bearing_bins = args.bearing_bins

# internal vars
keyring_key = "omero"
ttubules_area_label = "ttubules area"
ttubules_roi_label = 'ttubule'

def getPoints(shape):
    s = str(shape.getPoints().getValue())
    v = []
    for p in s.split():
        a = tuple([int(k) for k in p.split(',')])
        v.append(a)
    return v #np.array(v)
# def getPoints(shape):
#     s = str(shape.getPoints().getValue())
#     v = []
#     for p in s.split():
#         a = [int(k) for k in p.split(',')]
#         v.append(a)
#     return np.array(v)

####################################################
# main
####################################################

# establish connection to stats database
if not args.no_save:
    database = DatabaseInterface.get_database()

# open OMERO database
password = keyring.get_password(keyring_key, "%s@%s" % (username, host))
if password is None:
    print('\nPlease set login password using keyring')
    print(keyring_help)
    print("""In your case, username is currently set to '%s' and host is '%s'. So,
it should be

keyring set omero %s@%s

""" % (username, host, username, host))
    sys.exit(2)

print('Connecting as', username)
conn = BlitzGateway(username, password, host=host, secure=True)
conn.connect()

roi_service = conn.getRoiService()
updateService = conn.getUpdateService()

image = conn.getObject("Image", imageId)
print('Image:', image.getName(), " Dimensions: %d x %d x %d; channels=%d timepoints=%d" %
      (image.getSizeX(), image.getSizeY(), image.getSizeZ(), image.getSizeC(),image.getSizeT()))

size_x = image.getPixelSizeX(units="MICROMETER").getValue()       # e.g. 0.132
size_y = image.getPixelSizeY(units="MICROMETER").getValue()       # e.g. 0.132
print("Pixel Size X/Y:",  size_x, size_y)

# get detected ttubules
result = roi_service.findByImage(imageId, None)

ttubules = []
ttubules_area = None
for roi in result.rois:
    for s in roi.copyShapes():
        if type(s) == omero.model.PolygonI and \
           s.getTextValue().getValue() == ttubules_area_label:
            ttubules_area = getPoints(s)
            print('TTubules search area specified by ROI', s.getId().getValue())
        elif type(s) == omero.model.PolylineI and \
            s.getTextValue().getValue() == ttubules_roi_label:
            # tt = np.array(getPoints(s), dtype=float)
            # tt[:,0] *= size_x
            # tt[:,1] *= size_y
            ttubules.append(getPoints(s))

# Close OMERO connection
conn.close()

if ttubules_area is None:
    print('Error: ttubules search area is not specified as ROI. Select an area as a polygon and name it via comment as "%s"' % ttubules_area_label)
    conn.close()
    sys.exit(-2)

# get area
with PIL.Image.new('L', (image.getSizeX(), image.getSizeY()), 0) as img:
    PIL.ImageDraw.Draw(img).polygon(ttubules_area, fill=1, outline=1)
    ttubules_mask = np.array(img)

area = ttubules_mask.sum() * size_x * size_y
print('TTubules search area: %f microm^2' % area)
# plt.imshow(ttubules_mask)
# plt.show()

# draw ttubules on image
with PIL.Image.new('L', (image.getSizeX(), image.getSizeY()), 0) as img:
    for t in ttubules:
        PIL.ImageDraw.Draw(img).line(t, fill=128)
    ttubules_data = np.array(img)

# { Calculating average sarcomere length
def autocorr(x):
    result = np.correlate(x, x, mode='full')
    return result[int(np.round(result.size/2)):]

slc = ndimage.find_objects(ttubules_mask)[0]
cttm = ndimage.gaussian_filter(ttubules_data, (21, 0))
cttm = cttm[slc[0], slc[1]]
cy, cx = [i//2 for i in cttm.shape]
acf = signal.fftconvolve(cttm, cttm, mode='same')[cy-20:cy+20, cx-200:cx+200]#[:20, 0:cttm.shape[1]//4]
acf /= acf.max()
cttm = acf.mean(axis=0)
xx = np.arange(cttm.shape[0])
a, b = np.polyfit(xx, cttm, deg=1)
cttm = cttm - a*xx - b
sp = autocorr(cttm)

peaks, _ = find_peaks(sp, distance=1.5/size_x)
peaks_um = [size_x*p/(i+1) for i, p in enumerate(peaks)]
average_sl = np.mean(peaks_um)
print('Average SL =', average_sl)
if args.plot:
    plt.figure()
    plt.title(f'Average SL={average_sl:.3f}')
    plt.plot(sp)
    plt.plot(peaks, sp[peaks], "x")
    plt.plot(np.zeros_like(sp), "--", color="gray")
# }


if args.plot:
    plt.figure()
    plt.imshow(ttubules_data)

# scale ttubules coordinates
ttscaled = []
for t in ttubules:
    tt = np.array(t, dtype=float)
    tt[:,0] *= size_x
    tt[:,1] *= size_y
    ttscaled.append(tt)
ttubules = ttscaled


# calculate stats

# length
length_total = 0.0
for ps in ttubules:
    d = np.diff(ps, axis=0)
    length_total += np.sum( np.sqrt(np.sum(d**2, axis=1)) )
print('TTubules total length: %f microm' % length_total)

# orientation
bearing = []
bearing_lines = []
ox, oy = [], []
amp = 0.002
for ps in ttubules:

    for i in range(bearing_half_segment, ps.shape[0]-bearing_half_segment, 2*bearing_half_segment):
        v = ps[i-bearing_half_segment:i+bearing_half_segment+1, :]
        par = np.arange(v.shape[0])
        slope_x, intercept_x, r_value_x, p_value_x, std_err_x = stats.linregress(par,v[:,1])
        slope_y, intercept_y, r_value_y, p_value_y, std_err_y = stats.linregress(par,v[:,0])
        dx = slope_x
        dy = slope_y
        ox.append(abs(dx + (random.random()-0.5)*amp))
        oy.append(abs(dy + (random.random()-0.5)*amp))
        l = sqrt(dx*dx+dy*dy)
        b = 180/pi*atan2( abs(dy), abs(dx) )
        bearing.append([b, l])
        bearing_lines.append([v[:,1], v[:,0], int(b)])

if args.plot:
    plt.figure()
    plt.plot(ox, oy, '.')

# fractal dimension
if args.plot: plt.figure()
fd = fr.fractal_dimension(ttubules_data, minBoxSize=args.minedge+1, plotFigures=args.plot)
print("Minkowski–Bouligand dimension (computed): ", fd)

bearing = np.array(bearing)
#bhisto, bin_edges = np.histogram(bearing[:,0], range=[0, 90], bins=bearing_bins, weights = bearing[:,1], density=True)
bhisto, bin_edges = np.histogram(bearing[:,0], bins=bearing_bins, weights = bearing[:,1], density=True)
bin_edges_centers = (bin_edges[:-1]+bin_edges[1:])/2

# Cleanup database records for this image
if not args.no_save:
    database.query('DELETE FROM measurement_ttubules_properties WHERE image=:image',
                   image=imageId)
    database.query('DELETE FROM measurement_ttubules_orientation WHERE image=:image',
                   image=imageId)

    # record stats
    database.query('INSERT INTO measurement_ttubules_properties(image, name, value) VALUES(:im, :n, :v)',
                   im=imageId, n='length per area 1/um', v=length_total / area)
    database.query('INSERT INTO measurement_ttubules_properties(image, name, value) VALUES(:im, :n, :v)',
                   im=imageId, n='Minkowski–Bouligand dimension', v=fd)
    database.query('INSERT INTO measurement_ttubules_properties(image, name, value) VALUES(:im, :n, :v)',
                   im=imageId, n='Estimated sarcomere length um', v=average_sl)

    for i in range(bhisto.shape[0]):
        # if i == 0:
        #     a0, a1 = 2*bin_edges[i] - bin_edges[i+1], bin_edges[i+1]
        # elif i == bhisto.shape[0]-1:
        #     a0, a1 = bin_edges[i], 2*bin_edges[i+1] - bin_edges[i]
        # else:
        a0, a1 = bin_edges[i], bin_edges[i+1]

        database.query('INSERT INTO measurement_ttubules_orientation(image, angle0, angle1, density) VALUES(:im, :a0, :a1, :v)',
                       im=imageId,
                       a0 = a0,
                       a1 = a1,
                       v = bhisto[i])


if args.plot:
    if args.plot_bearing:
        import csv
        writer = csv.DictWriter(open('tubules.csv', 'w', newline=''),
                                fieldnames=["x", "y", "xend", "yend", "angle", "linenr"])
        writer.writeheader()

        plt.figure()
        colb = plt.cm.jet(np.linspace(0,1,91))
        lc = 0
        for l in bearing_lines:
            plt.plot(l[1], l[0], color=colb[l[2]])
            for i in range(len(l[0])-1):
                writer.writerow(dict(x=l[1][i], y=l[0][i], xend=l[1][i+1], yend=l[0][i+1], angle=l[2], linenr=lc))
            lc+=1
        plt.colorbar(mappable=cm.ScalarMappable(norm=colors.Normalize(0,90), cmap=plt.cm.jet))

    # bearing histogram
    plt.figure()
    plt.hist(bearing[:,0], bins=bearing_bins, weights = bearing[:,1], density=True)
    plt.plot(bin_edges_centers, bhisto, '.-')

    plt.show()
