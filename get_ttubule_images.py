#!/usr/bin/env python3

import argparse
import keyring, sys, os
import getpass, collections

import omero.model
from omero.gateway import BlitzGateway
from omero.rtypes import rdouble, rint, rstring

from iocbio.kinetics.io.db import DatabaseInterface

keyring_help = """
To set password for your login into OMERO, use command keyring:

keyring set omero username@host

where username is your OMERO username and host is OMERO host. For example,

keyring set omero user@omero.sysbio

While running the command, you should be prompted for the password.

"""

parser = argparse.ArgumentParser(description='Get images with TTubules',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog=keyring_help)

parser.add_argument('--host', default='omero.sysbio', help='OMERO host')
parser.add_argument('--username', default=getpass.getuser(), help='OMERO user login name')

args = parser.parse_args()

host = args.host
username = args.username

# internal vars
keyring_key = "omero"
tagOfInterest = 138
tagsToSkip = set([190])

ttubules_area_label = "ttubules area"
ttubules_roi_label = 'ttubule'

#########################################
def getTags(tagFolder):
    tags = {}
    for t in tagFolder.listTagsInTagset():
        i, txt = t.getId(), t.getTextValue()
        tags[txt] = i
        tags[i] = txt
    return tags

def getTagName(id):
    annotation = conn.getObject("Annotation", id)
    return annotation.getTextValue()


#########################################
################ MAIN ###################

password = keyring.get_password(keyring_key, "%s@%s" % (username, host))
if password is None:
    print('\nPlease set login password using keyring')
    print(keyring_help)
    print("""In your case, username is currently set to '%s' and host is '%s'. So,
it should be

keyring set omero %s@%s

""" % (username, host, username, host))
    sys.exit(2)

conn = BlitzGateway(username, password, host=host, secure=True)
conn.connect()
roi_service = conn.getRoiService()

# establish connection to iocbio database
db = DatabaseInterface.get_database()

print('Looking for images with tag:', getTagName(tagOfInterest))
print('Skip images with one of the following tags:', ' '.join([getTagName(t) for t in tagsToSkip]))
print()

fdetect = open('detect_all.sh', 'w')
fanalyze = open('analyze_all.sh', 'w')

for f in [fdetect, fanalyze]:
    f.write('#!/bin/bash\n')
    f.write('set -e\n')

areas_missing = ''

# all images with ttubules analyzed
ttub_analyzed = []
for q in db.query('select distinct  image from measurement_ttubules_properties'):
    ttub_analyzed.append(q.image)

# find all images
for project in conn.getObjects("Project"):
    for dataset in conn.getObjects("Dataset", opts={'project': project.getId()}):
        for image in conn.getObjects('Image', opts={'dataset': dataset.getId()}):
            tags = [ann.getId() for ann in image.listAnnotations()]
            if tagOfInterest in tags and not (tagsToSkip & set(tags)):

                # check if ttubules were detected already
                result = roi_service.findByImage(image.getId(), None)
                has_ttubules = False
                has_ttubules_area = False
                for roi in result.rois:
                    for s in roi.copyShapes():
                        if type(s) == omero.model.PolygonI and \
                           s.getTextValue().getValue() == ttubules_area_label:
                            has_ttubules_area = True
                        elif type(s) == omero.model.PolylineI and \
                           s.getTextValue().getValue() == ttubules_roi_label:
                            has_ttubules = True
                            break

                desc = '/'.join([project.getName(), dataset.getName(), image.getName(),
                                 str(image.getId()),
                                 'pixel=%0.1f' % image.getPixelSizeX(units="NANOMETER").getValue()
                                 ])

                if not has_ttubules_area:
                    ttext = 'Mark TTubules area'
                    areas_missing += desc + '\n'
                elif has_ttubules and image.getId() in ttub_analyzed:
                    ttext = 'TTubules detected and analyzed already - ALL DONE'
                elif has_ttubules:
                    ttext = 'TTubules detected but not analyzed'
                    fanalyze.write('echo ' + desc + '\n')
                    fanalyze.write('./analyze.py ' + str(image.getId()) + '\n')
                else:
                    ttext = 'TTubules not detected'
                    fdetect.write('echo ' + desc + '\n')
                    fdetect.write('./detect.py --omero --clear-rois --output output --ilastik ~/' + project.getName() + '.ilp ' + str(image.getId()) + '\n')
                print(desc, '->', ttext, ':', ' '.join([getTagName(t) for t in tags]))

print()
print('TTubule detection areas missing for:')
print(areas_missing)
print()

# Close OMERO connection
conn.close()
