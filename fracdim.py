# -----------------------------------------------------------------------------
# From https://en.wikipedia.org/wiki/Minkowski–Bouligand_dimension:
#
# In fractal geometry, the Minkowski–Bouligand dimension, also known as
# Minkowski dimension or box-counting dimension, is a way of determining the
# fractal dimension of a set S in a Euclidean space Rn, or more generally in a
# metric space (X, d).
# -----------------------------------------------------------------------------
import numpy as np
import skimage.io as io
import matplotlib.pyplot as plt

def fractal_dimension(data, minBoxSize=2, plotFigures=False):

    Z = (data > 0)

    # Only for 2d image
    assert(len(Z.shape) == 2)

    # From https://github.com/rougier/numpy-100 (#87)
    def boxcount(Z, k):
        S = np.add.reduceat(
            np.add.reduceat(Z, np.arange(0, Z.shape[0], k), axis=0),
                               np.arange(0, Z.shape[1], k), axis=1)

        # We count non-empty (0) and non-full boxes (k*k)
        # so, we estimate dimension of the borders
        return ((S > 0) & (S < k*k)).flatten().sum()
        # same, but written in other form: return len(np.where((S > 0) & (S < k*k))[0])


    # Minimal dimension of image
    p = min(Z.shape)

    # Greatest power of 2 less than or equal to p
    n = 2**np.floor(np.log(p)/np.log(2))

    # Extract the exponent
    n = int(np.log(n)/np.log(2))

    # Build successive box sizes (from 2**(n-2) down to 2**1)
    #sizes = np.unique( (2**np.arange(n-2, np.ceil(np.log(minBoxSize)/np.log(2)), -0.25)).astype(int) )
    sizes = np.unique( (2**np.arange(n-2, np.log(minBoxSize)/np.log(2), -0.1)).astype(int) )
    # this is for test, wrong sizes = np.unique( (2**np.arange(np.ceil(np.log(minBoxSize)/np.log(2)), 1, -0.25)).astype(int) )

    # Actual box counting with decreasing size
    counts = []
    szr = []
    for size in sizes:
        bc = boxcount(Z, size)
        if bc > 0:# and size < 65:
            counts.append(bc)
            szr.append(size)

    if plotFigures:
        plt.plot(np.log(szr), np.log(counts), 'o')
        
    # Fit the successive log(sizes) with log (counts)
    coeffs = np.polyfit(np.log(szr), np.log(counts), 1)
    return -coeffs[0]

if __name__ == "__main__":
    # I = io.imread("1024px-Sierpinski_triangle.svg.tif")/256.0
    # I = (I > 0.5)
    # print("Minkowski–Bouligand dimension (computed): ", fractal_dimension(I))
    # print("Haussdorf dimension (theoretical):        ", (np.log(3)/np.log(2)))

    I = io.imread("output/755_skeleton.tif")
    print(I.min(), I.max())
    print("Minkowski–Bouligand dimension (computed): ", fractal_dimension(I, minBoxSize=20, plotFigures=True))
    plt.show()

    # I = 0*np.ones((1024,1024))
    # sx = I.shape[0]//3  # if //4 we will get zero boxes on the border due to our split used in counting
    # I[sx:-sx,sx:-sx] = 1
    # print("Minkowski–Bouligand dimension (computed): ", fractal_dimension(I))
